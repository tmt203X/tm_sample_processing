//sample program #4
size(500, 500);
rect(0, 0, 50, 50);

translate(200, 200);
rect(0, 0, 50, 50);

line(-150,    0, 150,   0); // x axis
text("X", 150, 0); // X label

line(   0, -150,   0, 150); // y axis
text("Y", 0, 150); // Y label

rect(52, 52, 50, 50);

rotate(PI/6);
rect(52, 52, 50, 50);

rotate(PI/6);
rect(52, 52, 50, 50);

rotate(PI/6);
rect(52, 52, 50, 50);

rotate(PI/6);
rect(52, 52, 50, 50);
